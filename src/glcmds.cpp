#include <boost-server.hpp>
#include "config.h"
#include <accountutils.h>
#include "displaywindow.h"
#include "baseglobjects.h"
#include <string>
using boostserver::Command;
using boostserver::service;
using boostserver::client;
using boostserver::mythread;
using namespace RecArrUtils;

bool InitglCmds()
{
    Command* cmdobjlist=new Command();
    cmdobjlist->name="getobjs";
    cmdobjlist->func=[](boostserver::mythread* me,Command* cmd,const RecursionArray&  args,boostserver::client::ptr client)
    {
        RecursionArray result;
        result.add("count",std::to_string(programUI.objlist.size()));
        result.add("key","1");
        int i=0;
        RecursionArray result2;
        for(auto it = programUI.objlist.begin();it!=programUI.objlist.end();++it)
        {
            RecursionArray result3;
            result3.add("name",(*it)->name);
            result3.add("x",(*it)->center.x);
            result3.add("y",(*it)->center.y);
            result3.add("z",(*it)->center.z);
            result3.add("size",(*it)->_size);
            result2.add_child(std::to_string(i),result3);
            ++i;
        }
        result.add_child("objects",result2);
        client->do_write(toArcan(result));
    };
    service.cmdlist.push_back(cmdobjlist);
    Command* cmdobjmod=new Command();
    cmdobjmod->name="objmod";
    cmdobjmod->func=[](boostserver::mythread* me,Command* cmd,const RecursionArray&  args,boostserver::client::ptr client)
    {
        std::string name = args.get<std::string>("name","");
        AbstractObject* obj = client->thisObject;
        if(!name.empty())
        {
            for(auto i=programUI.objlist.begin();i!=programUI.objlist.end();++i)
            {
                if((*i)->name==name)
                {
                    obj = (*i);
                    break;
                }
            }
        }
        if(obj==nullptr)
        {
            ReturnCode(Fail);
            return;
        }
        obj->center.x = args.get<float>("x",obj->center.x);
        obj->center.y = args.get<float>("y",obj->center.y);
        obj->center.z = args.get<float>("z",obj->center.z);
        obj->_size = args.get<float>("size",obj->_size);
        obj->name = args.get<std::string>("newname",obj->name);
        if(obj->objtype==AbstractObject::SolidSphere)
        {
            SolidSphereObject* realobj = (SolidSphereObject*)obj;
            realobj->colorR = args.get<float>("colorR",realobj->colorR);
            realobj->colorG = args.get<float>("colorG",realobj->colorG);
            realobj->colorB = args.get<float>("colorB",realobj->colorB);
        }
        ReturnCode(OK);
    };
    service.cmdlist.push_back(cmdobjmod);
    Command* cmdcreateobj=new Command();
    cmdcreateobj->name="createobject";
    cmdcreateobj->func=[](boostserver::mythread* me,Command* cmd,const RecursionArray&  args,boostserver::client::ptr client)
    {
        std::string name = args.get<std::string>("name","");
        std::string type = args.get<std::string>("objtype","");
        AbstractObject* obj = nullptr;
        if(name.empty()) {ReturnCode(Fail); return;}
        if(type.empty()) {ReturnCode(Fail); return;}
        if(type=="sphere")
        {
            SolidSphereObject* obj2 = new SolidSphereObject();
            obj2->name=name;
            obj=obj2;
        }
        else if(type=="cube")
        {
            CubeObject* obj2 = new CubeObject();
            obj2->name=name;
            obj=obj2;
        }
        else {ReturnCode(Fail); return;}
        obj->center.x = args.get<float>("x",obj->center.x);
        obj->center.y = args.get<float>("y",obj->center.y);
        obj->center.z = args.get<float>("z",obj->center.z);
        obj->_size = args.get<float>("size",obj->_size);
        programUI.objlist.push_back(obj);
        ReturnCode(OK);
    };
    service.cmdlist.push_back(cmdcreateobj);
    Command* cmdobjrm=new Command();
    cmdobjrm->name="rmobj";
    cmdobjrm->func=[](boostserver::mythread* me,Command* cmd,const RecursionArray&  args,boostserver::client::ptr client)
    {
        std::string name = args.get<std::string>("name","");
        AbstractObject* obj = client->thisObject;
        bool sucsess = false;
        if(!name.empty())
        {
            for(auto i=programUI.objlist.begin();i!=programUI.objlist.end();++i)
            {
                if((*i)->name==name)
                {
                    sucsess = true;
                    programUI.objlist.erase(i);
                    delete (*i);
                    break;
                }
            }
        }
        if(sucsess){
            ReturnCode(OK);
        }
        else {
            ReturnCode(Fail);
        }
    };
    service.cmdlist.push_back(cmdobjrm);
    return 0;
}
