#include "displaywindow.h"
#include "baseglobjects.h"
#include <X11/Xlib.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void drawText(const char *text, int length, int x, int y)
{
    glMatrixMode(GL_PROJECTION);
    double *matrix = new double[16];
    glGetDoublev(GL_PROJECTION_MATRIX, matrix);
    glLoadIdentity();
    glOrtho(0,400,0,400,-5,5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x,y);
    for(int i=0; i<length; i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,(int)text[i]);
    }
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixd(matrix);
    glMatrixMode(GL_MODELVIEW);

}
void resize(int width,int height)
{
    glViewport(0,0,width,height);
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    //glOrtho(-5,5, -5,5, 2,12);
    //glFrustum(-5,5, -5,5, 2,12);
    gluPerspective( 45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f );
    gluLookAt( 0,0,5, 0,0,0, 0,1,0 );
    glMatrixMode( GL_MODELVIEW );
}
void display()
{
    programUI.CalculateFrameRate();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    /* remove next tree lines
         * and enter your code here
         */
    //glTranslated(0.01,0,0);
    //glColor3d(1,0,0);
    //изменились префиксы функций
    // и появились некоторые дополнительные параметры,
    // которые позволяют лучше контролировать
    // выполнение того или иного действия
    // в данном случае 15, 15 означает кол-во разбиений
    // вдоль оси Z и поперек, т.е. дискретность с которой будет рисоваться
    // сфера, чем выше эти параметры, тем более качественно будет сделана сфера,
    // но на это уйдет и больше времени
    //glutSolidSphere(1, 36,36);
    for(auto i=programUI.objlist.begin();i!=programUI.objlist.end();++i)
    {
        AbstractObject* r;
        Coord pos = (*i)->center;
        glTranslated(pos.x,pos.y,pos.z);
        (*i)->render();
        glTranslated(-pos.x,-pos.y,-pos.z);
    }
    drawText(std::to_string(programUI.FPS).c_str(),4,0.15,0.25);
    glutSwapBuffers();
}

DisplayWindow::DisplayWindow()
{

}
void DisplayWindow::CalculateFrameRate()
{
    static float framesPerSecond = 0.0f;    //наши фпс
    static char strFrameRate[50] = {0};     //Строка для вывода

    //Тут мы получаем текущий tick count и умножаем его на 0.001 для конвертации из миллисекунд в секунды.
    boost::posix_time::ptime currentTime = boost::posix_time::microsec_clock::local_time();

    //Увеличиваем счетчик кадров
    ++framesPerSecond;
    //Теперь вычтем из текущего времени последнее запомненное время. Если результат больше единицы,
    //это значит, что секунда прошла и нужно вывести новый FPS.
    if( ( currentTime - lastTime ) >= boost::posix_time::seconds(1))
    {
        //Устанавливаем lastTime в текущее время. Теперь оно будет использоватся как предидущее время
        //для след. секунды.
        lastTime = currentTime;

        // Установим FPS для вывода:
        FPS=framesPerSecond;

        //Сбросим FPS
        framesPerSecond = 0;
    }
}

void DisplayWindow::initUI(int argc, char **argv)
{
    /*Display *d;
       Window w;
       XEvent e;
       char *msg = "Hello, World!";
       int s;

       if( ( d = XOpenDisplay( getenv("DISPLAY" ) ) ) == NULL ) {  // Соединиться с X сервером,
          printf( "Can't connect X server: %s\n", strerror( errno ) );
          exit( 1 );
       }
       s = DefaultScreen( d );
       w = XCreateSimpleWindow( d, RootWindow( d, s ),     // Создать окно
                                10, 10, 200, 200, 1,
                                BlackPixel( d, s ), WhitePixel( d, s ) );
       XSelectInput( d, w, ExposureMask | KeyPressMask );  // На какие события будем реагировать?
       XMapWindow( d, w );                                 // Вывести окно на экран
       while( 1 ) {                                        // Бесконечный цикл обработки событий
          XNextEvent( d, &e );
          if( e.type == Expose ) {                         // Перерисовать окно
             XFillRectangle( d, w, DefaultGC( d, s ), 20, 20, 10, 10 );
             XDrawString( d, w, DefaultGC( d, s ), 50, 50, msg, strlen( msg ) );
          }
          if( e.type == KeyPress )                         // При нажатии кнопки - выход
             break;
       }
       XCloseDisplay( d );                                 // Закрыть соединение с X сервером*/
    // тут только префиксы поменялись
    // назначения функций можете посмотреть в
    // разделе по библиотеке GLAUX
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(50, 10);
    glutInitWindowSize(400, 400);
    glutInitDisplayMode( GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE );
    glutCreateWindow( "GLUT Template" );
    glutIdleFunc(display);
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glEnable(GL_DEPTH_TEST);
    glutMainLoop();
}
