TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lboost_system -lboost_filesystem -lpthread -lmysqlclient -lmysqlpp -lX11 -lGL -lGLU -lglut
SOURCES += main.cpp \
    boost-server.cpp \
    utils.cpp \
    basecmds.cpp \
    testcmds.cpp \
    basefuncions.cpp \
    service.cpp \
    tcpclient.cpp \
    memcached.cpp \
    displaywindow.cpp \
    abstractobject.cpp \
    coord.cpp \
    glcmds.cpp \
    baseglobjects.cpp

HEADERS += \
    boost-server.hpp \
    accountutils.h \
    recarray.h \
    config.h \
    basefuncions.h \
    tcpclient.h \
    memcached.h \
    displaywindow.h \
    abstractobject.h \
    coord.h \
    baseglobjects.h
