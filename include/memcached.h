#ifndef MEMCACHED_H
#define MEMCACHED_H
#include <tcpclient.h>
#include <string>
class MemcachedConnect : public TcpClient
{
public:
    MemcachedConnect();
    TcpClient::ptr sock;
    void connect(std::string ip, int port)
    {
        sock = start(boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(ip),port));
        sock->do_write("version");
    }
    virtual void on_read(const error_code & err, size_t bytes)
    {
        if ( !err)
        {
            std::string copy(read_buffer_, bytes - 1);
            std::cout << "server echoed our2 " << message_ << ": "<< (copy == message_ ? "OK" : "FAIL") << std::endl;
        }
        stop();
    }
};

#endif // MEMCACHED_H
