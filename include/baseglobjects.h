#ifndef SOLIDSPHEREOBJECT_H
#define SOLIDSPHEREOBJECT_H
#include "abstractobject.h"

class SolidSphereObject : public AbstractObject
{
public:
    SolidSphereObject();
    virtual void render();
    virtual void processTick();
    virtual ~SolidSphereObject();
    float colorR,colorG,colorB;
};

class CubeObject : public AbstractObject
{
public:
    CubeObject();
    virtual void render();
    virtual void processTick();
    virtual ~CubeObject();
    float cubeVertexArray[8][3];
    float cubeColorArray[8][3];
    unsigned char cubeIndexArray[6][4];
    float colorR,colorG,colorB;
};

#endif // SOLIDSPHEREOBJECT_H
