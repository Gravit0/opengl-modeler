#ifndef DISPLAYWINDOW_H
#define DISPLAYWINDOW_H
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/bind.hpp>
#include "abstractobject.h"
#include <list>
class DisplayWindow
{

public:
    float FPS;
    DisplayWindow();
    void initUI(int argc, char **argv);
    void CalculateFrameRate();
    boost::posix_time::ptime lastTime;
    std::list<AbstractObject*> objlist;
};
extern DisplayWindow programUI;
#endif // DISPLAYWINDOW_H
