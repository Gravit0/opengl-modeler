#ifndef ABSTRACTOBJECT_H
#define ABSTRACTOBJECT_H

#include "coord.h"
#include <thread>
#include <vector>
class TimeThread;
class AbstractObject
{
public:
    virtual void render() = 0;
    virtual void processTick() = 0;
    virtual void init();
    Coord center;
    TimeThread* timespace;
    double _size,temperature;
    std::string name;
    AbstractObject();
    virtual ~AbstractObject();
    enum objtypes
    {
        SolidSphere,
        Cube
    };
    objtypes objtype;
};
class TimeThread
{
protected:
    unsigned int speed;
    std::thread* ptr;
    std::vector<AbstractObject*> objects;
public:
    void addSpeed(unsigned int light);
    bool rmSpeed(unsigned int light);
    void start();
    void processTick();
};

#endif // ABSTRACTOBJECT_H
